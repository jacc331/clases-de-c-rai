#include <stdlib.h>
#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <math.h>
#include <cstdlib> 

float X1,X2,R1,R2,R3,R4,R5,A,B,C;

main(){
	
	system("color 9f");

printf("\n\n	Ecuacion para resolver la resolvente de segundo grado.");
	
printf("\n\n	La ecuacion es la siguiente:");

printf("\n\n       		ax2 + bx + c = 0 ");

printf("\n\n	Ingrese el valor de (a):");
scanf("%f",&A);

printf("\n	El valor de (a) es: %.2f",A);

printf("\n\n	Ingrese el valor de (b):");
scanf("%f",&B);

printf("\n	El valor de (b) es: %.2f",B);

printf("\n\n	Ingrese el valor de (c):");
scanf("%f",&C);

printf("\n	El valor de (c) es: %.2f",C);

R1=pow(B,2)-(4*A*C);

printf("\n\n	El valor de R (Raiz) es: %.2f",R1);


R2=sqrt(R1);

R3=-1*B;

R4=2*A;

X1=(R3+R2)/R4;


if(R1<0){
	printf("\n\n	El resultado es inadmisible, debido a que la raiz no admite negativos.\n\n");
}

else {
printf("\n\n		El resultado del valor de (x1) (+) es: %.2f",X1);	

X2=(R3-R2)/R4;

printf("\n\n		El resultado del valor de (x2) (-) es: %.2f\n\n",X2);	
}
	system("\npause");
    	
	return 0;

}
